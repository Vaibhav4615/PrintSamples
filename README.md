# Printing samples of audio after fft transform
Dependencies:

- Tkinter must be installed

Install Requirements:

```
pip install -r requirements.txt
```

To test run :

```
python main.py
```
