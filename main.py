import matplotlib.pyplot as plt
import numpy as np
import wave
import sys
from scan import record_to_file
from scipy.fftpack import fft

plt.figure(1)
record_to_file()

spf = wave.open('voice.wav','r')

#Extract Raw Audio from Wav File
signal = spf.readframes(-1)
signal = np.fromstring(signal, 'Int16')
fourier = fft(signal)

for i in range(len(fourier)):
    print(fourier[i], end=' ', flush = True)

#If Stereo
if spf.getnchannels() == 2:
    print('Just mono files')
    sys.exit(0)

print('\nFourier series printed')


